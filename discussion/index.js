// COMMENTS
// - To write comments, we just have to press the buttons "control" and "forward slash" 
/*
	- To write multi-line comments, we can add two asterisks inside of the 
	two forward slashes and write the multi-line comments inside of them. 
*/ 
/*
	Types of Comments in JavaScript:
	1. The single line comment denoted by two slashes
	2. The multi-line comment denoted by two slashes and two asterisks. The contents of the comments are between the two asterisks. 
*/ 

// VARIABLE
/*
	- Variables contain values that can be changed over the execution time of the program. 
	- To declare a variable, we use the let keyword. 

	
	Guides in writing variables (also applies to constant): 
	1. Use the "let" keyword followed by a chosen variable name. 
		e.g. "let" inventoryPrice = 1000;
	2. Use the "assignment operator (=)" to assign a value. 
		e.g. let inventoryPrice "=" 1000;
	3. Variable names should start with a lowercase character. In JS, we use camelCase for multiple words. We avoid using capital letters as there are keywords in JS that start with capital letters. Do not use spaces. 

		e.g. 
		let FirstName = "Michael"; - bad variable name
		let firstName = "Michael"; - good variable name

	4. Variable names should describe what they contain.  
		e.g. let "inventoryPrice" = 1000;
*/

/* 
	DECLARING VARIABLES

	- In the english language, declaring means to say something, to announce, to make known publicly. 
	- When we declare a variable, we tell our devices that a variable name is created and is ready to store data
	- Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value is "not defined".
	- let is a keyword that is usually used in declaring a variable. 

	Syntax: 

		let variableName;
*/ 
let myName;
console.log(myName)

/*
	INITIALIZING VARIABLES

	- When we initialize a variable, we give the variable an "initial" or a starting value. 

	Syntax: 

		let variableName = value;
*/ 
let greeting = "Hello";
console.log(greeting);
myName = "Nehemiah Ellorico";
console.log(myName);

/*
	REASSIGNING VARIABLES

	- Reassigning a variable means changing its initial or previous value into another value. 

	Syntax: 

		variableName = newValue
*/ 
myName = "Choco Martin";
console.log(myName)

/*
	NOTES on Variables
*/

/*
	1. VARIABLE DECLARATION
	- We can only re-declare our variables once. We cannot use the "let" two times for the same variable. We can only reassign, but we cannot, redeclare. 

		For example: 

			let band = "Boys Like Girls";
			let band = "Paramore";
			console.log(band)

			- This will return an error. We can only reassign, but we cannot re-declare. 

			The right way to do it is: 

			let band = "Parokya ni Edgar";
			band = "Kamikazee"
			console.log(band)
*/

/*
	2. VAR VS. LET
	- **When we search the internet regarding JavaScript, we can see that sometimes they use "var" for variables instead of let. 

	VAR
	- "var" is an old way of declaring variables. 
	- It was a feature of Javascript from 1997. 
	- The version of JavaScript that we are talking about is called "ECMAScript1" or "ES1"
	- The version of JavaScript that we have now is ES6. 

	LET
	- "let" is a new feature. 
	- It was introduced last 2015 in "ES6" or "ECMAScript6" version of JavaScript

	VAR VS. LET
	- "var" allows hoisting, "let" does not allow hoisting (const does not too)
	- "var" has a global scope, "let" does not have a global scope. "let" only has a block scope. (const too)

		What is hoisting? 
		- Hoisting is when we can execute a code before it is declared.

		What do you mean by scope? 
		- Javascript has blocks of code. 
		- When we say that a variable is global, we can use it on other blocks of code
		- When we say that a variable is local, we can only use it inside a block of code. It cannot be used on other functions/blocks of code.
*/ 

// let productName = 'desktop computer';
// console.log(productName);
// productName = "cellphone";
// console.log(productName);

// let productPrice = 500;
// console.log(productPrice);
// productPrice = 450;
// console.log(productPrice);


// CONSTANTS
/*
	- Use constants for values that will not change. 
	- For constants declared for arrays and objects, the properties can be changed. 
		- why? The constants in arrays and objects refer to the constant reference to the array or object, not the value. 
			e.g. a section of students, in real life. Section 10 pa din. May transferee lang. unlike sa name ng bata, alam mong siya lang yun.
	- A constant are block scoped. 
	- A constant does not allow hoisting. 
	- 
*/ 
// const deliveryFee = 30;
// console.log(deliveryFee);


// DATA TYPES

// 1. STRINGS
/*
- Strings are a series of characters that create a word, a phrase, a sentence, or anything related to text.
- Strings in Javascript can be written using a single quote ('') or a double quote ("")
- On other programming languages, only the double quote can be used for creating strings. 
*/ 
let country = 'Philippines';
let province = "Metro Manila";

console.log(country)
console.log(province)

	// Notes on Strings: 

	// 3.1.1 Concatenating Strings
	// Multiple string values can be combines to create a single string using the "+" symbol. 
let fullAddress = "I live in " + province + ', ' + country;
console.log(fullAddress)

	// 3.1.2 Escape Character (\) in strings in combination with other characters can produce different effects
	// "\n" refers to creating a new line in between text
let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

	// 3.1.3 Using the double quotes can enable us to use single quotes in texts without using escape characters
let message = "John's employees went home early";
console.log(message)
message = 'John\'s employees went home early';
console.log(message)

	/*
		MINI-ACTIVITY NO. 1: 
		In the activity folder, create an index.html and an index.js
		1. Create a variable to store the following details. 
			- First Name
			- Last last Name
		2. Mimic the expected output
		3. Send the screenshot of the value of the variables on the console
	*/ 

// 2. NUMBERS
// Numbers include integers/whole numbers, decimal numbers/fraction, and exponential notation. 

	// 3.2.1 Integers/Whole Numbers
let headcount = 26;
console.log(headcount)

	// 3.2.2 Decimal Numbers/Fractions
let grade = 98.7
console.log(grade)

	// 3.3.3 Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

	// 3.3.4 Combining numbers and strings/text
console.log("John's grade last quarter is " + grade)

// 3. BOOLEAN
/*
	- Boolean values are logical values. 
	- Boolean values can contain either "true" or "false"
*/ 
let isMarried = false;
let isGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);
console.log(isMarried);
console.log(isGoodConduct);

// 4. OBJECTS

	// Arrays
	/*
		- They are a special kind of data that stores multiple values. 
		- They are a special type of an object. 
		- They can store different data types but is normally used to store similar data types.
	*/ 

	// Syntax: 
		// let/const arrayName = [ElementA, ElementB, ElementC...]
let grades = [ 98.7, 92.1, 90.2, 94.6 ];
console.log(grades)
console.log(grades[0])

let userDetails = ["John", "Smith", 32, true ]
console.log(userDetails);

	// Object Literals
	/*
		- Objects are another special kind of data type that mimics real world objects/items
		- They are used to create complex data that contains pieces of information that are relevant to each other. 
		- Every individual piece of information if called a property of the object

		Syntax: 

			let/const objectName = {
				propertyA: value,
				propertyB: value
			}
	*/ 

let personDetails = {
	fullName: 'Juan Dela Cruz',
	age: 35, 
	isMarried: false,
	contact: ['+639876543210', '024785425'],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
}
console.log(personDetails)

	// 5. NULL
/*
	- It is used to intentionally express the absence of a value. 
*/ 
let spouse = null;
console.log(spouse)


// 6. UNDEFINED
/*
	- Represents the state of a variable that has been declared but without an assigned value
*/ 
let vacationPlace;
console.log(vacationPlace)
